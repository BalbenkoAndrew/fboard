package com.omctf.fboard.service;

import com.omctf.fboard.controller.request.LoginRequest;
import com.omctf.fboard.dao.LoginDao;
import com.omctf.fboard.dao.SessionDao;
import com.omctf.fboard.exception.FutureBoardException;
import com.omctf.fboard.model.Sessions;
import com.omctf.fboard.model.User;
import com.omctf.fboard.utils.Constants;
import com.omctf.fboard.utils.MD5;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@Service
public class AuthenticationService {

    private LoginDao loginDao;
    private SessionDao sessionDao;
    private static final MD5 md5 = new MD5();

    public AuthenticationService (LoginDao loginDao, SessionDao sessionDao) {
        this.loginDao = loginDao;
        this.sessionDao = sessionDao;
    }

    public void login(LoginRequest loginRequest, HttpServletResponse httpServletResponse) throws NoSuchAlgorithmException, FutureBoardException {
        String login = loginRequest.getLogin();
        String password = md5.hashingPasswordMD5(loginRequest.getPassword());

        boolean userExist = loginDao.getUserByLoginAndPassword(login, password);
        if (userExist) {
            User user = loginDao.getUserByLogin(login);
            String cookie =  generateCookie(login, 16);
            Sessions sessions = new Sessions(cookie, user);
            sessionDao.saveToken(sessions);
            httpServletResponse.addCookie(new Cookie(Constants.JAVASESSIONID, cookie));
        } else {
            throw new FutureBoardException("Login or Password incorrect");
        }
    }

    public static String generateCookie(String login, int tokenLength) {
        SecureRandom random = new SecureRandom();
        byte[] tokenBytes = new byte[tokenLength];
        random.nextBytes(tokenBytes);
        return login + Base64.getEncoder().encodeToString(tokenBytes);
    }
}
