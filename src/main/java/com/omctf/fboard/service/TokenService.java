package com.omctf.fboard.service;

import com.omctf.fboard.dao.SessionDao;
import com.omctf.fboard.model.User;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

    private SessionDao sessionDao;

    public TokenService(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    public User checkToken(String token) {
        return sessionDao.getUserByToken(token);
    }
}
