package com.omctf.fboard.service;

import com.omctf.fboard.controller.request.RegistrationRequest;
import com.omctf.fboard.controller.response.RegistrationResponse;
import com.omctf.fboard.dao.RegistrationDao;
import com.omctf.fboard.dao.SessionDao;
import com.omctf.fboard.model.Sessions;
import com.omctf.fboard.model.User;
import com.omctf.fboard.utils.Constants;
import com.omctf.fboard.utils.MD5;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.security.NoSuchAlgorithmException;

@Service
public class RegistrationService {

    private RegistrationDao registrationDao;
    private SessionDao sessionDao;

    private static final MD5 md5 = new MD5();

    public RegistrationService (RegistrationDao registrationDao, SessionDao sessionDao) {
        this.registrationDao = registrationDao;
        this.sessionDao = sessionDao;
    }

    @Transactional
    public RegistrationResponse registration(RegistrationRequest registrationRequest, HttpServletResponse httpServletResponse) throws NoSuchAlgorithmException {
        String login = registrationRequest.getLogin();
        String password = registrationRequest.getPassword();

        String md5Password = md5.hashingPasswordMD5(password);

        User user = new User(registrationRequest.getFirstName(), registrationRequest.getLastName(), registrationRequest.getEmail(), login, md5Password);
        Integer id = registrationDao.saveUser(user);

        String cookie =  AuthenticationService.generateCookie(registrationRequest.getLogin(), 16);
        Sessions sessions = new Sessions(cookie, user);
        sessionDao.saveToken(sessions);

        httpServletResponse.addCookie(new Cookie(Constants.JAVASESSIONID, cookie));

        return new RegistrationResponse(user.getFirstName(), user.getLastName(), user.getRole());
    }
}
