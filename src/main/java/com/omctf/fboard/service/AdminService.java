package com.omctf.fboard.service;

import com.omctf.fboard.controller.response.EditUserResponse;
import com.omctf.fboard.dao.AdminDao;
import com.omctf.fboard.dao.UserDao;
import com.omctf.fboard.exception.FutureBoardException;
import com.omctf.fboard.model.User;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    private UserDao userDao;
    private AdminDao adminDao;

    public AdminService(UserDao userDao, AdminDao adminDao) {
        this.userDao = userDao;
        this.adminDao = adminDao;
    }

    public EditUserResponse editUserRole(int userId) throws FutureBoardException {
        User user = userDao.getUserById(userId);

        if (user.getRole().equals("ROLE_ADMIN")) {
            throw new FutureBoardException("User already admin");
        }

        user.setRole("ROLE_ADMIN");
        adminDao.editUserRole(user);

        return new EditUserResponse(user.getFirstName(), user.getLastName(), user.getRole());
    }
}
