package com.omctf.fboard.utils;

import com.omctf.fboard.model.Advertising;
import com.omctf.fboard.model.Basket;
import com.omctf.fboard.model.Sessions;
import com.omctf.fboard.model.User;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

public class HibernateUtils {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    public static SessionFactory buildSessionFactory() {
        try {
            Configuration conf = new Configuration();
            conf.addAnnotatedClass(Advertising.class);
            conf.addAnnotatedClass(Basket.class);
            conf.addAnnotatedClass(Sessions.class);
            conf.addAnnotatedClass(User.class);
            return conf.configure("hibernate.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
