package com.omctf.fboard.utils;

public enum AdvertisingType {
    BUILDING("Building"),
    TRANSPORT("Transport"),
    CAFE("Cafe"),
    ANDROID_ASSISTANT("Android assistant"),
    STATION("Station");

    private String advertisingType;

    private AdvertisingType(String advertisingType) {
        this.advertisingType = advertisingType;
    }

    public String getAdvertisingType() {
        return advertisingType;
    }
}
