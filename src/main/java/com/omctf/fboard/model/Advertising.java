package com.omctf.fboard.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "advertising")
public class Advertising {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    private User user;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "content_link")
    private String contentLink;

    @NotNull
    @Column(name = "longitude")
    private double longitude;

    @NotNull
    @Column(name = "latitudel")
    private double latitudel;

    @Column(name = "show_time")
    private LocalTime showTime = LocalTime.of(0, 3);

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "advertising_space_id")
    private AdvertisingSpace advertisingSpace;

    @NotNull
    @Column(name = "is_show")
    private boolean isShow;

    @ManyToOne
    private Basket basket;

    @Column(name = "is_approve")
    private boolean isApprove;

    public Advertising() {
    }

    public Advertising(User user, double longitude, double latitudel, @NotNull String title, @NotNull String contentLink,
                       @NotNull boolean isShow, AdvertisingSpace advertisingSpace) {
        this.user = user;
        this.title = title;
        this.contentLink = contentLink;
        this.isShow = isShow;
        this.longitude = longitude;
        this.latitudel = latitudel;
        this.advertisingSpace = advertisingSpace;
        this.isApprove = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentLink() {
        return contentLink;
    }

    public void setContentLink(String contentLink) {
        this.contentLink = contentLink;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitudel() {
        return latitudel;
    }

    public void setLatitudel(double latitudel) {
        this.latitudel = latitudel;
    }

    public AdvertisingSpace getAdvertisingSpace() {
        return advertisingSpace;
    }

    public void setAdvertisingSpace(AdvertisingSpace advertisingSpace) {
        this.advertisingSpace = advertisingSpace;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public boolean isApprove() {
        return isApprove;
    }

    public void setApprove(boolean approve) {
        isApprove = approve;
    }

    public LocalTime getShowTime() {
        return showTime;
    }

    public void setShowTime(LocalTime showTime) {
        this.showTime = showTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Advertising that = (Advertising) o;
        return id == that.id &&
                Double.compare(that.longitude, longitude) == 0 &&
                Double.compare(that.latitudel, latitudel) == 0 &&
                isShow == that.isShow &&
                isApprove == that.isApprove &&
                Objects.equals(user, that.user) &&
                Objects.equals(title, that.title) &&
                Objects.equals(contentLink, that.contentLink) &&
                Objects.equals(showTime, that.showTime) &&
                Objects.equals(advertisingSpace, that.advertisingSpace) &&
                Objects.equals(basket, that.basket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, title, contentLink, longitude, latitudel, showTime, advertisingSpace, isShow, basket, isApprove);
    }
}
