package com.omctf.fboard.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "basket")
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToMany(mappedBy = "basket", cascade = CascadeType.ALL)
    private List<Advertising> advertising = new ArrayList<>();

    public Basket() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Advertising> getAdvertising() {
        return advertising;
    }

    public void setAdvertising(List<Advertising> advertising) {
        this.advertising = advertising;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Basket basket = (Basket) o;
        return id == basket.id &&
                Objects.equals(advertising, basket.advertising);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, advertising);
    }
}
