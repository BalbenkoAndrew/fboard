package com.omctf.fboard.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(mappedBy = "user")
    private List<Advertising> advertising;

    @OneToOne(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Sessions sessions;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Email
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "login")
    private String login;

    @NotNull
    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String role;

    @Column(name = "last_add_advertising")
    private LocalDateTime lastAddAdvertising;

    @Column(name = "link_click_counter")
    private int linkClickCounter;

    public User() {
    }

    public User(String firstName, String lastName, String email, @NotNull String login, @NotNull String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.advertising = new ArrayList<>();
        this.linkClickCounter = 0;
        this.role  = "ROLE_USER";
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Sessions getSessions() {
        return sessions;
    }

    public void setSessions(Sessions sessions) {
        this.sessions = sessions;
    }

    public List<Advertising> getAdvertising() {
        return advertising;
    }

    public void setAdvertising(List<Advertising> advertising) {
        this.advertising = advertising;
    }

    public int getLinkClickCounter() {
        return linkClickCounter;
    }

    public void setLinkClickCounter(int linkClickCounter) {
        this.linkClickCounter = linkClickCounter;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public LocalDateTime getLastAddAdvertising() {
        return lastAddAdvertising;
    }

    public void setLastAddAdvertising(LocalDateTime lastAddAdvertising) {
        this.lastAddAdvertising = lastAddAdvertising;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return linkClickCounter == user.linkClickCounter &&
                Objects.equals(id, user.id) &&
                Objects.equals(advertising, user.advertising) &&
                Objects.equals(sessions, user.sessions) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(role, user.role) &&
                Objects.equals(lastAddAdvertising, user.lastAddAdvertising);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, advertising, linkClickCounter, sessions, firstName, lastName, email, login, password, role, lastAddAdvertising);
    }
}
