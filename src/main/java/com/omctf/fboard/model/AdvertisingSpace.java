package com.omctf.fboard.model;

import com.omctf.fboard.utils.AdvertisingType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "advertising_space")
public class AdvertisingSpace {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    @OneToOne(
            mappedBy = "advertisingSpace",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Advertising advertising;

    @NotNull
    @Column(name = "advertising_type")
    private AdvertisingType advertisingType;

    public AdvertisingSpace(@NotNull Advertising advertising, @NotNull AdvertisingType advertisingType) {
        this.advertising = advertising;
        this.advertisingType = advertisingType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Advertising getAdvertising() {
        return advertising;
    }

    public void setAdvertising(Advertising advertising) {
        this.advertising = advertising;
    }

    public AdvertisingType getAdvertisingType() {
        return advertisingType;
    }

    public void setAdvertisingType(AdvertisingType advertisingType) {
        this.advertisingType = advertisingType;
    }
}
