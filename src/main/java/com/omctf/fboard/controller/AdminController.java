package com.omctf.fboard.controller;

import com.omctf.fboard.controller.response.EditUserResponse;
import com.omctf.fboard.exception.FutureBoardException;
import com.omctf.fboard.service.AdminService;
import com.omctf.fboard.utils.Constants;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/fboard")
public class AdminController {

    private AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("/{user_id}")
    public EditUserResponse editUserRole(@CookieValue(Constants.JAVASESSIONID) String cookie, @PathVariable("user_id") int userId) throws FutureBoardException {
        return adminService.editUserRole(userId);
    }
}
