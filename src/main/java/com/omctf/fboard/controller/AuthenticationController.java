package com.omctf.fboard.controller;

import com.omctf.fboard.controller.request.LoginRequest;
import com.omctf.fboard.exception.FutureBoardException;
import com.omctf.fboard.service.AuthenticationService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/api/fboard")
public class AuthenticationController {

    private AuthenticationService authenticationService;

    public AuthenticationController (AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void login (@RequestBody LoginRequest loginRequest,
                       HttpServletResponse httpServletResponse) throws NoSuchAlgorithmException, FutureBoardException {
        authenticationService.login(loginRequest, httpServletResponse);
    }
}
