package com.omctf.fboard.controller;

import com.omctf.fboard.controller.request.RegistrationRequest;
import com.omctf.fboard.service.RegistrationService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/api/fboard")
public class RegistrationController {

    private RegistrationService registrationService;

    public RegistrationController (RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @PostMapping(value = "/registration", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void registration (@RequestBody RegistrationRequest registrationRequest,
                              HttpServletResponse httpServletResponse) throws NoSuchAlgorithmException {
        registrationService.registration(registrationRequest, httpServletResponse);
    }
}
