package com.omctf.fboard.exception;

public class FutureBoardException extends Exception {

    private String message;

    public FutureBoardException (String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
