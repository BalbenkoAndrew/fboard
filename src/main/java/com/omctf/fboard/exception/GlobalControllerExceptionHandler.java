package com.omctf.fboard.exception;

import com.omctf.fboard.controller.response.FailureResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@ControllerAdvice
public class GlobalControllerExceptionHandler implements ResponseErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(FutureBoardException.class)
    @ResponseBody
    public ResponseEntity<FailureResponse> handleCmdException(FutureBoardException ex) {
        FailureResponse failureResponse = new FailureResponse(ex.getMessage());
        return new ResponseEntity<>(failureResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

    }
}
