package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;

public interface RegistrationDao {
    Integer saveUser(User user);
}
