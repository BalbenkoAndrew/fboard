package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
public class AdminDaoImpl extends BaseDaoImpl implements AdminDao {
    @Override
    public void editUserRole(User user) {
        try (Session session = getSession()){
            session.update(user);
        }
    }
}
