package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;

import java.util.Optional;

public interface LoginDao {
    public boolean getUserByLoginAndPassword(String login, String password);
    public User getUserByLogin(String login);
}
