package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
public class UserDaoImpl extends BaseDaoImpl implements UserDao {
    @Override
    public User getUserById(int id) {
        try (Session session = getSession()) {
            return session.get(User.class, id);
        }
    }
}
