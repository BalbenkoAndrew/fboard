package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;

public interface AdminDao {
    public void editUserRole(User user);
}
