package com.omctf.fboard.dao;

import com.omctf.fboard.utils.HibernateUtils;
import org.hibernate.Session;

public class BaseDaoImpl {

    protected Session getSession() {
        return HibernateUtils.getSessionFactory().openSession();
    }
}
