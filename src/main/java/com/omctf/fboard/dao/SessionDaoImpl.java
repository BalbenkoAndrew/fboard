package com.omctf.fboard.dao;

import com.omctf.fboard.model.Sessions;
import com.omctf.fboard.model.User;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

@Component
public class SessionDaoImpl extends BaseDaoImpl implements SessionDao {

    @Override
    public void saveToken(Sessions sessions) {
        try (Session session = getSession()) {
            session.getTransaction().begin();
            session.save(sessions);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getUserByToken(String cookie) {
        try (Session session = getSession()) {
            Query query = session.createQuery("SELECT u FROM User u JOIN Sessions s ON u.id = s.user_id WHERE s.cookie = :cookie");
            query.setParameter("cookie", cookie);
            return (User) query.getSingleResult();
        }
    }
}
