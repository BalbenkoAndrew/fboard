package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
public class RegistrationDaoImpl extends BaseDaoImpl implements RegistrationDao {

    @Override
    public Integer saveUser(User user) {
        try (Session session = getSession()) {
            session.getTransaction().begin();
            Integer id = (Integer) session.save(user);
            session.getTransaction().commit();
            return id;
        }
    }
}
