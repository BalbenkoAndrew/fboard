package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;

public interface UserDao {
    public User getUserById(int id);
}
