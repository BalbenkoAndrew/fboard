package com.omctf.fboard.dao;

import com.omctf.fboard.model.Sessions;
import com.omctf.fboard.model.User;

public interface SessionDao {
    public void saveToken(Sessions session);
    public User getUserByToken(String token);
}
