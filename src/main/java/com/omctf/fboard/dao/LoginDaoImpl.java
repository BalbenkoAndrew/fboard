package com.omctf.fboard.dao;

import com.omctf.fboard.model.User;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.Query;

@Component
public class LoginDaoImpl extends BaseDaoImpl implements LoginDao {

    @Override
    public boolean getUserByLoginAndPassword(String login, String password) {
        try (Session session = getSession()) {
            Query query = session.createQuery("SELECT CASE count(*) WHEN 0 THEN FALSE ELSE TRUE END " +
                    "FROM User u WHERE u.login = :login AND u.password = :password");
            query.setParameter("login", login);
            query.setParameter("password", password);
            return (boolean) query.getSingleResult();
        }
    }

    @Override
    public User getUserByLogin(String login) {
        try (Session session = getSession()) {
            Query query = session.createQuery("from User u where u.login = :login");
            query.setParameter("login", login);
            return (User) query.getSingleResult();
        }
    }
}
