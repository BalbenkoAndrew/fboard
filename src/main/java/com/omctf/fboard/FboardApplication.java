package com.omctf.fboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(FboardApplication.class, args);
    }

}
